Microsecond Timer [[Doc]](https://godoc.org/gitlab.com/tapir/utime)
=================

* `utime` has a single function `utime.Now()` which returns time in seconds with at least microsecond resolution.
* This was necessary because Go only provides millisecond resolution timer in Windows systems. So in Windows, this library uses [QPC](https://msdn.microsoft.com/en-us/library/windows/desktop/dn553408%28v=vs.85%29.aspx) to achieve a resolution of 1us.

Example
------------

Example implementation of a game loop with fixed timestep independent of the
FPS.

```go
package main

import (
	"fmt"
	"gitlab.com/tapir/utime"
	"time"
)

func main() {
	dt := 0.03
	lastTime := utime.Now()
	accumulator := 0.0
	totalTime := 0.0

	for totalTime < 2 {
		newTime := utime.Now()
		frameTime := newTime - lastTime
		lastTime = newTime
		totalTime += frameTime

		// Avoid spiral of death on very low fps
		if frameTime > 0.25 {
			frameTime = 0.25
		}

		accumulator += frameTime
		for accumulator >= dt {
			update(dt)
			accumulator -= dt
		}

		alpha := accumulator / dt
		render(alpha)
	}
}

func update(dt float64) {
	// Wait 15 milliseconds
	time.Sleep(15 * time.Millisecond)
	fmt.Println("Updating: ", dt)
}

func render(alpha float64) {
	fmt.Println("Rendering: ", alpha)
}
```

