package utime

import (
	"syscall"
	"unsafe"
)

var (
	dll            = syscall.MustLoadDLL("kernel32.dll")
	queryCounter   = dll.MustFindProc("QueryPerformanceCounter")
	queryFrequency = dll.MustFindProc("QueryPerformanceFrequency")
)

// Now returns a timestamp in seconds
func Now() float64 {
	var (
		count int64
		freq  int64
	)
	queryCounter.Call(uintptr(unsafe.Pointer(&count)))
	queryFrequency.Call(uintptr(unsafe.Pointer(&freq)))
	return float64(count) / float64(freq)
}
