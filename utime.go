// +build !windows

package utime

import (
	"time"
)

// Now returns a timestamp in seconds
func Now() float64 {
	return float64(time.Now().UnixNano()) / float64(time.Second)
}
